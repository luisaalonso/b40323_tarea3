import algoritmos
import numpy as np

print "Digite la ruta que contiene el archivo con numeros"
ruta = raw_input()

try:
    lista = np.loadtxt(ruta)
    print "Que algoritmos de ordenamiento quiere usar: quicksort, burbuja o insercion?"
    algoritmo = raw_input()
    if algoritmo == 'quicksort':
        lista = algoritmos.ord_quicksort(lista)
    if algoritmo == 'burbuja':
        lista = algoritmos.ord_burbuja(lista)
    if algoritmo == 'insercion':
        lista = algoritmos.ord_insercion(lista)
    print "La lista ordenada quedo: ", lista
except IOError:
    print "La cadena texto no corresponde a una ruta"
    exit()
except ValueError:
    print "El archivo contiene datos no convertibles a float"
    exit()
