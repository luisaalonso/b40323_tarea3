import time 
import random
import algoritmos
import numpy as np

def lista_random(rango):
    lista_random = open('lista', 'w')
    while rango > 0:
        numero = random.random()*100
        lista_random.write(str(numero)+"\n")
        rango -= 1
    lista_random.close()

def lista_ordenada(rango):
    lista_ordenada = open('lista', 'w')
    i = 0
    while rango > i:
        numero = i
        lista_ordenada.write(str(numero)+"\n")
        i += 1
    lista_ordenada.close()

def lista_casi_ordenada(rango):
    lista_casi_ordenada = open('lista', 'w')
    for i in range(rango):
        if i%2 == 0:
            numero = i
        else:
            numero = random.random()*100
        lista_casi_ordenada.write(str(numero)+"\n")
    lista_casi_ordenada.close()

def performance_algoritmos(caso, rango):
    lista_algoritmos = ['quicksort', 'insercion', 'burbuja', 'sort']
    #datos = {}
    Duracion = {}
    resultados = {}
    if caso == lista_random:
        lista_random(rango)
    if caso == lista_ordenada:
        lista_ordenada(rango)
    if caso == lista_casi_ordenada:
        lista_casi_ordenada(rango)
    for algoritmo in lista_algoritmos:
        if algoritmo == 'quicksort':
            lista = np.loadtxt('lista')
            inicio = time.time()
            lista = algoritmos.ord_quicksort(lista)
            fin = time.time()
            duracion = fin - inicio
        if algoritmo == 'burbuja':
            lista = np.loadtxt('lista')
            inicio = time.time()
            lista = algoritmos.ord_burbuja(lista)
            fin = time.time()
            duracion = fin - inicio
        if algoritmo == 'insercion':
            lista = np.loadtxt('lista')
            inicio = time.time()
            lista = algoritmos.ord_insercion(lista)
            fin = time.time()
            duracion = fin - inicio
        if algoritmo == 'sort':
            lista = np.loadtxt('lista')
            inicio = time.time()
            lista = np.sort(lista)
            fin = time.time()
            duracion = fin - inicio            
        Duracion[algoritmo] = {'Duracion': duracion}
        resultados[algoritmo] = {'Resultado': lista}
        datos = [Duracion, resultados]
    print datos[0]

caso = raw_input('Caso: lista_random, lista_ordenada, lista_casi_ordenada? ')
rango = raw_input('Lista de rango? ')

performance_algoritmos(caso, rango)
