def ord_insercion(lista):
    i = 0
    while i <(len(lista)-1):
        if lista[i+1]< lista[i]:
            cambiar_posicion(lista, i+1)
        i += 1
    return lista
 
def cambiar_posicion(lista, n):
    v = lista[n]
    j = n
    while j > 0 and v < lista[j-1]:
	if v < lista[j-1]:
	        lista[j] = lista[j-1]
        j -= 1
    lista[j] = v

def ord_burbuja(lista):
     i = 0
     while i < len(lista):
         j = i
         while j < len(lista):
             if lista[i] > lista[j]:
                 temp = lista[i]
                 lista[i] = lista[j]
                 lista[j] = temp
             j += 1     
         i += 1     
     return lista 

def ord_quicksort(lista):
   quicksort(lista, 0, len(lista)-1)
   return lista

def quicksort(lista, primero, ultimo):
   if primero<ultimo:
       split = partir(lista, primero, ultimo)
       quicksort(lista, primero, split-1)
       quicksort(lista, split+1, ultimo)

def partir(lista, primero, ultimo):
   pivote = lista[primero]
   punt_izq = primero+1
   punt_drcha = ultimo
   while True:
       while punt_izq <= punt_drcha:
           if lista[punt_izq] <= pivote:
               punt_izq += 1
           else:
               break

       while lista[punt_drcha] >= pivote:
           if punt_drcha >= punt_izq:
               punt_drcha -= 1
           else:
               break

       if punt_drcha < punt_izq:
           break
       else:
           temp = lista[punt_izq]
           lista[punt_izq] = lista[punt_drcha]
           lista[punt_drcha] = temp

   temp = lista[primero]
   lista[primero] = lista[punt_drcha]
   lista[punt_drcha] = temp
   return punt_drcha
